FROM debian:buster

# Set some variables
ENV USERNAME='root'
ENV PASSWORD=''

# Set the frontend to noninteractive for apt-get
# ARG DEBIAN_FRONTEND=noninteractive

# Installing the required packages
# Nginx | MariaDB (MySQL) | Php-FPM

RUN	apt-get update -yq && apt-get dist-upgrade -yq && apt-get install -yq apt-utils
RUN apt-get install -yq debconf nano wget procps unzip nginx mariadb-server \
php7.3 php-mysql php-fpm php-pdo php-gd php-cli php-mbstring

# Expose ports for use

EXPOSE 80 443

# Set the working directory
WORKDIR /var/www/ft_server

# Copy config files into container
COPY ./srcs/nginx.conf /etc/nginx/sites-available/ft_server

# Setup everything
RUN \
GRN="\033[32;1m" && RST="\033[m" \
&& \
printf "${GRN}=== === Making symbolic link for nginx configuration === ===${RST}\n" && \
/bin/bash -c "ln -s /etc/nginx/sites-available/ft_server /etc/nginx/sites-enabled" \
&& \
printf "${GRN}=== === Generating SSL certificate === ===${RST}\n\033[33m" && \
openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
-subj '/CN=s19' \
-keyout /etc/ssl/nginx-selfsigned.key -out /etc/ssl/nginx-selfsigned.crt \
&& \
printf "${GRN}=== === Downloading & Extracting phpmyadmin === ===${RST}\n" && \
wget --progress=bar:force: https://files.phpmyadmin.net/phpMyAdmin/5.0.3/phpMyAdmin-5.0.3-english.tar.gz -O phpMyAdmin-5.0.3-english.tar.gz && \
tar --full-time -xzf phpMyAdmin-5.0.3-english.tar.gz && \
mv -v phpMyAdmin-5.0.3-english/ phpmyadmin/ && \
rm -f phpMyAdmin-5.0.3-english.tar.gz \
&& \
printf "${GRN}=== === Downloading & Extracting wordpress === ===${RST}\n" && \
wget --progress=bar:force: https://wordpress.org/latest.tar.gz -O latest.tar.gz && \
tar --full-time -xzf latest.tar.gz && \
rm -f latest.tar.gz \
&& \
printf "${GRN}=== === Setting files ownership & permissions === ===${RST}\n" && \
chown -R www-data:www-data ../**/** && chmod -R 755 /var/www/* && chmod 777 /etc/nginx/sites-available/ft_server \
&& \
printf "${GRN}=== === Removing default nginx files === ===${RST}\n" && \
rm -rf /etc/nginx/sites-available/default /etc/nginx/sites-enabled/default

# Copy config files into container
COPY ./srcs/config.inc.php phpmyadmin/config.inc.php
COPY ./srcs/wp-config.php ./wordpress/wp-config.php
COPY ./srcs/start.sh /start.sh
# Start
ENTRYPOINT /bin/bash /start.sh