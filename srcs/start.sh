#!/bin/bash

# RED="\033[31;1m"
# GRN="\033[32;1m"
# YEL="\033[33;1m"
# RST="\033[0m"

start_services()
{
    printf "${GRN}=== === Starting Services === ===${RST}\n"
    printf "${GRN} + NGINX + ${RST}\n"
    service nginx start
    printf "${GRN} + MySQL + ${RST}\n"
    service mysql start
    printf "${GRN} + Php-FPM + ${RST}\n"
    service php7.3-fpm start
    printf "${GRN}All services started successfully${RST}\n"
}

create_wordpress_db()
{
    printf "${GRN}=== === Creating WordPress DB === ===${RST}\n"
    echo "CREATE DATABASE wordpress;"| mysql -u root --skip-password
    echo "GRANT ALL PRIVILEGES ON wordpress.* TO 'root'@'localhost' WITH GRANT OPTION;"| mysql -u root --skip-password
    echo "FLUSH PRIVILEGES;"| mysql -u root --skip-password
    echo "update mysql.user set plugin='' where user='root';"| mysql -u root --skip-password
    printf "${GRN}=== WordPress DB created successfully${RST}\n"

}

# get_package_gzip ()
# {
#     local archive=${1##*/}
#     printf $YEL"=== Downloading ${archive} archive"$RST"\n"
#     curl -# $1 -o $archive
#     local extracted=$(tar -tzf ${archive} | head -1 | cut -f1 -d"/")
#     printf $YEL"+++ Extracting ${archive} archive${RST}\n"
#     tar -xzf $archive
#     printf $YEL"--- Removing ${archive} archive"$RST"\n"
#     rm -rf $archive
#     if [[ ! $extracted == $2 ]]; then
#         printf $YEL". . Renaming ${extracted} to ${2} ${RST}\n"
#         mv -f $extracted $2;
#     fi
# }
# get_third_party()
# {
#     printf $GRN"=== === PhpMyAdmin === ==="$RST"\n"
#     get_package_gzip https://files.phpmyadmin.net/phpMyAdmin/5.0.3/phpMyAdmin-5.0.3-english.tar.gz phpmyadmin
#     printf $GRN"=== === WordPress === ==="$RST"\n"
#     get_package_gzip https://wordpress.org/latest.tar.gz wordpress
# }

get_ip()
{
    printf "\n\033[44;37m\n=== Container IP :\n %s\n===\033[0m\n\n" $(ip addr show eth0 | grep inet | awk '{ print $2; }' | sed 's/\/.*$//')
}

start_services
create_wordpress_db
get_ip
sleep infinity & wait